import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreationBlogComponent } from './creation-blog.component';
import { MatTabsModule, MatCardModule, MatIconModule, MatTooltipModule, MatSnackBarModule } from '@angular/material';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { HighlightModule } from 'ngx-highlightjs';
import yaml from 'highlight.js/lib/languages/yaml';
import typescript from 'highlight.js/lib/languages/typescript';
import dockerfile from 'highlight.js/lib/languages/dockerfile';
import nginx from 'highlight.js/lib/languages/nginx';
import { DockerComponent } from './docker/docker.component';
import { CicdComponent } from './cicd/cicd.component';
import { ClipboardModule } from 'ngx-clipboard';

function hljsLanguages() {
  return [
    {name: 'typescript', func: typescript},
    {name: 'yaml', func: yaml},
    {name: 'dockerfile', func: dockerfile},
    {name: 'nginx', func: nginx},
  ];
}

@NgModule({
  declarations: [CreationBlogComponent, GettingStartedComponent, DockerComponent, CicdComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    MatCardModule,
    HighlightModule.forRoot({languages: hljsLanguages}),
    ClipboardModule,
    MatIconModule,
    MatTooltipModule,
    MatSnackBarModule
  ]
})
export class CreationBlogModule { }
