import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackbar: MatSnackBar) { }

  public notifyCopied() {
    this.snackbar.open('Copied source to clipboard.', null, {duration: 3000});
  }
}
