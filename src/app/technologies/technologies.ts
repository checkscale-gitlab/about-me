export interface Technology {
  name: string;
  description: string;
  professional_experience: number;
  personal_experience: number;
}

export const USERS = [
  {name: 'SQL', description: 'The only constant.', professional_experience: 3, personal_experience: 5},
  {name: 'Angular', description: 'My favorite web technology.', professional_experience: 2, personal_experience: 2},
  {name: 'C#', description: 'Love the language, hate Windows.', professional_experience: 1, personal_experience: 3},
  {name: 'Python', description: 'My gateway into Programming.', professional_experience: 1, personal_experience: 4},
  {name: 'Docker', description: 'Makes my life easier.', professional_experience: 2, personal_experience: 2},
  {name: 'NginX', description: 'Beautifully fast.', professional_experience: 2, personal_experience: 2},
  {name: 'PHP/Laravel', description: 'A new frontier.', professional_experience: 1, personal_experience: 1},
  {name: 'Vim', description: 'Better than emacs.', professional_experience: 2, personal_experience: 3},
  {name: 'Node.js', description: 'Very easy to use.', professional_experience: 2, personal_experience: 2},
  {name: 'JavaScript', description: 'No types? No thanks.', professional_experience: 2, personal_experience: 5},
  {name: 'Ionic', description: '"It works on my machine".', professional_experience: 1, personal_experience: 1},
  {name: 'MongoDB', description: 'Nice for small personal projects.', professional_experience: 1, personal_experience: 2},
  {name: 'Linux', description: 'My preferred work environment.', professional_experience: 2, personal_experience: 4},
];
